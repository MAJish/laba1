﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ТФЯиГ_ЛР2
{
    class Program
    {
        static void file()
        {
            Console.WriteLine("Укажите полный путь к файлу, содержащему набор правил.");
            string FileName = Console.ReadLine();
            while (FileName.Length < 5 || FileName.Length >= 5 && FileName.Substring(FileName.Length - 4) != ".txt")
            {
                Console.WriteLine("Укажите корректный путь к файлу, содержащему набор правил.");
                FileName = Console.ReadLine();
            }
            string[] FileCopy = copy(FileName);
            if (FileCopy.Length >= 5)
            {
                string[] terminal = terminalchek(FileCopy);
                string[] neterminal = neterminalchek(FileCopy);
                bool res = proverk(terminal, neterminal);
                if (res == false)
                {
                    Console.WriteLine("Неправильно заполнены массивы") ;
                    Environment.Exit(0);
                }
                List<string> prav = creat(FileCopy, FileCopy[3]);
                Dictionary<string, string> Rules = Program.dictionar(FileCopy, FileCopy[3]);
                string terminals = Program.terminal(prav, Rules);
                Console.WriteLine(terminals);
            }
            else
            {
                Console.WriteLine("В указанном файле недостаточно условий.");
            }
            Console.WriteLine("Желаете загрузить новые правила грамматики? [Д/Н]");
        }

        static string[] copy(string FileName)
        {
            string FileLine;
            int count = 0;
            StreamReader LSR = new StreamReader(FileName);
            while ((FileLine = LSR.ReadLine()) != null)
            {
                count++;
            }
            LSR.Close();
            string[] FileCopy = new string[count];
            int i = 0;
            StreamReader nam = new StreamReader(FileName);
            while ((FileLine = nam.ReadLine()) != null)
            {
                FileCopy[i] = FileLine;
                i++;
            }
            nam.Close();
            return FileCopy;
        }

        static string[] terminalchek(string[] FileCopy)
        {
            string[] ts;
            int count = 0;
            int uniq = 0;
            string[] trw = new string[0];
            ts = FileCopy[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < ts.Length - 1; i++)
            {
                for (int j = i + 1; j < ts.Length; j++)
                {
                    if (ts[j] == ts[i] && ts[j] != "|")
                    {
                        ts[j] = "|";
                        count++;
                    }
                }
            }
            trw = new string[ts.Length - count];
            for (int i = 0; i < ts.Length; i++)
            {
                if (ts[i] != "|")
                {
                    trw[uniq] = ts[i];
                    uniq++;
                }
            }
            return trw;
        }

        static string[] neterminalchek(string[] FileCopy)
        {
            string[] neter;
            int count = 0;
            int uniq = 0;
            string[] net = new string[0];
            neter = FileCopy[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < neter.Length - 1; i++)
            {
                for (int j = i + 1; j < neter.Length; j++)
                {
                    if (neter[j] == neter[i] && neter[j] != "|")
                    {
                        neter[j] = "|";
                        count++;
                    }
                }
            }
            net = new string[neter.Length - count];
            for (int i = 0; i < neter.Length; i++)
            {
                if (neter[i] != "|")
                {
                    net[uniq] = neter[i];
                    uniq++;
                }
            }
            if (!net.Contains(FileCopy[2]))
            {
                string[] NTT = new string[net.Length + 1];
                NTT[0] = FileCopy[2];
                for (int i = 0; i < net.Length; i++)
                {
                    NTT[i + 1] = net[i];
                }
                net = NTT;
            }
            return net;
        }

        static bool proverk(string[] terminal, string[] neterminal)
        {
            
            for (int i = 0; i < neterminal.Length; i++)
            {
                for (int j = 0; j < terminal.Length; j++)
                {
                    if (terminal[j] == neterminal[i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        static List<string> creat(string[] FileCopy, string d)
        {
            d = d.Split(new string[] { " = " }, StringSplitOptions.RemoveEmptyEntries)[0];
            string PE = "";
            List<string> PExpression = new List<string>();
            string[] PLine;
            PLine = FileCopy[3].Split(new string[] { " = " }, StringSplitOptions.RemoveEmptyEntries);
            PE = PLine[1];
            for (int i = 0; i < FileCopy.Length; i++)
            {
                if (i > 3)
                {
                    string[] KeyValue = FileCopy[i].Split(new string[] { " = " }, StringSplitOptions.RemoveEmptyEntries);
                    if (KeyValue[0] == d)
                    {
                        PE = PE + " | " + KeyValue[1];
                    }
                }
            }
            var RStringNumber = new Random();
            string[] ELine = PE.Split(new string[] { " | " }, StringSplitOptions.RemoveEmptyEntries);
            string[] FELine = ELine[RStringNumber.Next(0, ELine.Length)].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < FELine.Length; i++)
            {
                PExpression.Add(FELine[i]);
            }
            return PExpression;
        }

        static Dictionary<string, string> dictionar(string[] FileCopy, string d)
        {
            d = d.Split(new string[] { " = " }, StringSplitOptions.RemoveEmptyEntries)[0];
            Dictionary<string, string> Rules = new Dictionary<string, string>();
            for (int i = 0; i < FileCopy.Length; i++)
            {
                if (i > 4)
                {
                    string[] KeyValue = FileCopy[i].Split(new string[] { " = " }, StringSplitOptions.RemoveEmptyEntries);
                    if (KeyValue[0] != d)
                    {
                        if (Rules.ContainsKey(KeyValue[0]))
                        {
                            string NewValue = Rules[KeyValue[0]] + " | " + KeyValue[1];
                            Rules[KeyValue[0]] = NewValue;
                        }
                        else
                        {
                            Rules.Add(KeyValue[0], KeyValue[1]);
                        }
                    }
                }
            }
            return Rules;
        }

        static string terminal(List<string> pe, Dictionary<string, string> dir)
        {
            string TerminalExpression = "";
            int LoopingCheck = 0;
            var RStringNumber = new Random();
            for (int i = 0; i < pe.Count; i++)
            {
                if (dir.ContainsKey(pe[i]))
                {
                    string[] RStringArray = dir[pe[i]].Split(new string[] { " | " }, StringSplitOptions.RemoveEmptyEntries);
                    string[] SSymbolsArray = RStringArray[RStringNumber.Next(0, RStringArray.Length)].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    pe.RemoveAt(i);
                    for (int j = 0; j < SSymbolsArray.Length; j++)
                    {
                        pe.Insert(i + j, SSymbolsArray[j]);
                    }
                    i--;
                }
                LoopingCheck++;
                if (LoopingCheck == 1000)
                {
                    break;
                }
            }
            for (int i = 0; i < pe.Count; i++)
            {
                TerminalExpression = TerminalExpression + pe[i];
            }
            return TerminalExpression;
        }

        static void Main(string[] args)
        {
            try
            {
                string input;
                Console.WriteLine("Здравствуйте! Желаете загрузить правила грамматики? [Д/Н]");
                input = Console.ReadLine();
                while ( input != "Н")
                {
                    switch (input)
                    {
                        case ("Д"):
                            file();
                            input = Console.ReadLine();
                            Console.WriteLine("Хорошего дня!");
                            Environment.Exit(0);
                            break;
                        case ("Н"):
                            Console.WriteLine("Хорошего дня!");
                            Environment.Exit(0);
                            break;
                        default:
                            while (input != "Д" && input != "Н")
                            {
                                Console.WriteLine("Пожалуйста, укажите корректный ответ.");
                                Console.WriteLine("Желаете загрузить правила грамматики? [Д/Н]");
                                input = Console.ReadLine();
                            }
                            while (input == "Д")
                            {
                                file();
                                input = Console.ReadLine();
                            }
                            Console.WriteLine("Раьота закончена!");
                            Environment.Exit(0);
                            break;
                    }
                }
                
                
            }
            catch (Exception E)
            {
                Console.WriteLine("Произошла ошибка: ", E.Message);
            }
        }

    }
}
